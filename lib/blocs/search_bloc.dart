import 'package:flutter/cupertino.dart';
import 'package:grocery/models/product.dart';
import 'package:grocery/services/auth.dart';
import 'package:grocery/services/database.dart';

class SearchBloc {
  final Database database;
  final AuthBase auth;

  SearchBloc({@required this.database,@required this.auth});

  ///get searched items
  Stream<List<Product>> getSearchedProducts(String data) {
    return database.getSearchedDataFromCollection('products', data).map(
        (snapshots) => snapshots.docs
            .map((snapshot) => Product.fromMap(snapshot.data(), snapshot.id))
            .toList());
  }

  ///Remove all cart items
  Future<void> removeCart() async {
    await database.removeCollection("users/${auth.uid}/cart");
  }
}
