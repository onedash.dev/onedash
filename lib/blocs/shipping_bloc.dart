import 'package:flutter/cupertino.dart';
import 'package:grocery/models/shipping_method.dart';
import 'package:grocery/services/database.dart';
import 'package:rxdart/rxdart.dart';

class ShippingBloc {
  final Database database;
  final String uid;

  ShippingBloc({@required this.database, @required this.uid});

  ///Get shipping methods
  Stream<List<ShippingMethod>> _getShippingMethods() {
    final snapshot = database.getDataFromCollection("shipping");

    return snapshot.map((event) =>
        event.docs.map((e) => ShippingMethod.fromMap(e.data(),e.id)).toList());
  }

  ///Get shipping methods with selected shipping and combine them using RxDart
  Stream<List<ShippingMethod>> getShippingMethods() {
    return Rx.combineLatest2(_getShippingMethods(), _getSelectedShipping(),
            (List<ShippingMethod> shippingMethods, String selectedShipping) {
              bool isSelected=false;
              shippingMethods.forEach((element) {
                if(element.id==selectedShipping){
                  element.selected = true;
                  isSelected=true;
                }else{
                  element.selected = false;

                }
              });

              if(shippingMethods.length!=0 && !isSelected){
                shippingMethods[0].selected=true;
              }

      return shippingMethods;
    });
  }

  ///Get selected shipping index
  Stream<String> _getSelectedShipping() {
    final snapshot =
        database.getDataFromDocument("users/$uid/settings/shipping");

    return snapshot.map((document) =>
        (document.data() == null) ? null : document.data()['selected'].toString());
  }

  ///Update selected shipping index
  Future<void> setSelectedShipping(String value) async {
    await database.setData({'selected': value}, 'users/$uid/settings/shipping');
  }
}
